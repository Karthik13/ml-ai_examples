library(shiny)
library(shinydashboard)

dashboardPage(skin = "green",
  dashboardHeader(title = "Column layout"),
  dashboardSidebar(
    tags$head(tags$style(HTML('.shiny-server-account { display: none; }'))),
    sidebarMenu(
     menuItem("Dashboard", tabName = "dashboard", icon = icon("dashboard")),
      menuItem("Widgets", tabName = "widgets", icon = icon("th"))
    )
  ),
  dashboardBody("Calendar from Font-Awesome:", icon("calendar"),
                "Cog from Glyphicons:", icon("cog", lib = "glyphicon"),
    tags$head(tags$style(HTML('
        .skin-blue .main-header .logo {
          background-color: #3c8dbc;
        }
        .skin-blue .main-header .logo:hover {
          background-color: #3c8dbc;
        }
      '))),
  
    tabItems(
      # First tab content
      tabItem(tabName = "dashboard",
              fluidRow(
                box(background = "maroon",plotOutput("plot1", height = 250)),
                
                box(
                  title = "Inputs", background = "black",
                  "Box content here", br(), "More box content",
                  sliderInput("slider", "Slider input:", 1, 100, 50)
                  #textInput("text", "Text input:")
                )
              )
      ),
      
      # Second tab content
      tabItem(tabName = "widgets",
              fluidRow(
                # A static infoBox
                infoBox("New Orders", 10 * 2, icon = icon("credit-card")),
                # Dynamic infoBoxes
                infoBoxOutput("progressBox" ),
                infoBoxOutput("approvalBox"),
                fluidRow(
                  # Clicking this will increment the progress amount
                  box(width = 4, actionButton("count", "Increment progress"))
                )
              ),
                sidebarSearchForm(textId = "searchText", buttonId = "searchButton",
                                  label = "Search..."),
              
              fluidRow(
                tabBox(
                  title = "First tabBox",
                  # The id lets us use input$tabset1 on the server to find the current tab
                  id = "tabset1", height = "250px",
                  tabPanel("Tab1", "First tab content"),
                  tabPanel("Tab2", "Tab content 2")
                ),
                "Calendar from Font-Awesome:", icon("calendar"),
                "Cog from Glyphicons:", icon("cog", lib = "glyphicon")              
              ),
              fluidRow(
                tabBox(
                  # Title can include an icon
                  title = tagList(shiny::icon("gear"), "tabBox status"),
                  tabPanel("Tab1",
                           "Currently selected tab from first box:",
                           verbatimTextOutput("tabset1Selected")
                  ),
                  tabPanel("Tab2", "Tab content 2")
                )
              )
      )
    )
  )
)