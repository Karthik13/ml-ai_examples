library(shiny)
shinyUI(fluidPage(
  titlePanel(title="This is my first shiny app"),
  sidebarLayout(
  sidebarPanel(
  ("personnel information"),
  textInput("subject","enter the subject",""),
  textInput("description","enter the description","")),
  mainPanel("this is main panel",
  textOutput("subject"),
  textOutput("description")
)
))
)