library(shiny)

shinyServer(
  function(input,output)
  {
    output$subject<-renderText(input$subject)
 output$description<-renderText(input$description)
  }
)
