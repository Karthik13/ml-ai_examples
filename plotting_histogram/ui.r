library(shiny)
shinyUI(fluidPage(
  titlePanel(title = h4("demonstration of plots",align="center")),
  sidebarLayout(
    sidebarPanel (
      selectInput("var","select the variable from iris dataset",choices = c("Sepal.Length"=1,"Sepal.Width"=2,"Petal.Length."=3,"Petal.Width"=4)),
      br(),
      sliderInput("bin","select the number of bins for histogram",min=5,max=25,value=15),
      br(),
      radioButtons("color","select the color",choices = c("green","red","yellow"),selected = "green")
      
    ),
    mainPanel(plotOutput("myhist"))
  )
))