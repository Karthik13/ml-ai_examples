library(shiny)
shinyServer(
  
  function(input,output){
    
    output$myhist <- renderPlot({
      
      colm<-as.numeric(input$var)
      
      hist(iris[,colm],breaks=seq(0,max(iris[,colm]),l=input$bin+1),col=input$color)
    })
  }
)